package get

import (
	"bytes"
	"testing"
)

func TestGet(t *testing.T) {
	type args struct {
		url string
	}
	tests := []struct {
		name string
		args args
	}{
		{name: "Test1", args: args{url: "https://srijan-sengupta.github.io"}},
		//{name: "Test2", args: args{url: "https://bitbucket.org"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Get(tt.args.url)
		})
	}
}

func TestGetAndWriteToFile(t *testing.T) {
	type args struct {
		url string
	}
	tests := []struct {
		name    string
		args    args
	}{
		{name: "GetAndWrite Test", args: args{url: "https://github.com/RiiConnect24/RiiConnect24-Patcher/releases/download/v1.4.3-v1.1.2/RiiConnect24Patcher-Unix.zip"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			out := &bytes.Buffer{}
			GetAndWriteToFile(tt.args.url, out)
			//fmt.Println(out.String())
		})
	}
}
