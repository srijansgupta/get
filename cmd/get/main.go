package main

import (
	"flag"
	"fmt"
	"os"

	"bitbucket.org/srijansgupta/get"
)

var(
	url *string
	out *string
)

func init(){
	url = flag.String("u","","The url to get file from...")
	out = flag.String("f",os.Stdout.Name(),"The file name to write the downloaded file")
	
}
func main(){
	flag.Parse()
	if *url == "" {
		flag.Usage()
		os.Exit(1)
	}
	if *out == "" {
		get.Get(*url)
	}else{
		file, err:= os.Create(*out)
		checkError(err)
		get.GetAndWriteToFile(*url,file)
	}
	fmt.Printf("\n")
}

func checkError(e error){
	if e != nil {
		panic(e)
	}
}