# Get

### Goals:
- to make a low dependency api to download files from web
- a cross-platform native tool to download things easily
- executable about 5MB of size which requires no installations

### Technology Used:

- Golang 1.17

### Installation:

Simply download the releases in the repository based on your operating system and Architecture

### List of supported Platforms:

- Linux
    - amd64
    - 386
    - arm
    - arm64

- Windows
    - amd64
    - 386
    - arm
    - arm64

- Mac OSX
    - amd64
    - arm64

