package get

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

// Get is used to download a file from the given url and prints it to the os.Stdout
func Get(url string)(){
	resp, e := http.Get(url)
	checkerr(e)
	defer resp.Body.Close()
	_,er := io.Copy(os.Stdout,resp.Body)
	checkerr(er)
}

func checkerr(err error){
	if err != nil {
		fmt.Println(err.Error())
	}
}

func GetAndWriteToFile(url string, out io.Writer){
	resp, e := http.Get(url)
	checkerr(e)
	defer resp.Body.Close()
	counter := &Counter{}
	_,er := io.Copy(out, io.TeeReader(resp.Body,counter))
	checkerr(er)
}

type Counter struct{
	TotalCount uint64
}

func (c *Counter) Write(p []byte)(int,error){
	n := len(p)
	c.TotalCount += uint64(n)
	c.PrintProgress()
	return n,nil
}

func (c *Counter) PrintProgress(){
	fmt.Printf("\rDownloading... %d completed.", c.TotalCount)
}